package org.alto.token;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.alto.utility.ControllerSendMessageHSM;
import org.alto.utility.HexStringConverter;
import org.alto.utility.Utility;
import org.alto.hsm.HSMSecure;
import org.json.simple.JSONObject;

public class FormatPreservingToken 
{
	// Normally FPE is for SSNs, CC#s, etc, nothing too big
    private static final int MAX_N_BYTES = 128 / 8;
    private static BigInteger modulus = null;
	//private static final BigInteger modulus  = new BigInteger("10000000000000000");
	private static byte[] byteDPK;
	private static final byte[] tweak = "".getBytes();
	
	private static ControllerSendMessageHSM hsm;
	private static HexStringConverter conv = new HexStringConverter();
	private static Utility util = new Utility();
	private static HSMSecure hsm_secure;
	
	private String hostHSM;
	private int portHSM;
	
	public FormatPreservingToken(String hostHSM, int portHSM){
		this.hostHSM = hostHSM;
		this.portHSM = portHSM;	
		hsm = new ControllerSendMessageHSM(hostHSM, portHSM);
	}
	
	public static JSONObject generateFormatPreservingToken(String card_number) throws Exception {
		JSONObject joResult = new JSONObject();
		
		try {
			String DPK = hsm_secure.generateDPK();
		    //String DPK  = "6DBE9AFB99B3D13CB51A91E3947053F1DB04E99B67AC2233";
		    byteDPK     = DPK.getBytes(); 
		    String data = card_number;   
		    
		    String modulus_param = Utility.rightPad("1", data.length() + 1, "0");
		    modulus = new BigInteger(modulus_param);
		    
		    String token = encrypt(new BigInteger(data, 10), byteDPK, tweak, modulus).toString();
		    String IMK_AC = hsm_secure.generateIMK_AC();
		
		    if(token.length() > 0) {
		    	joResult.put("token", token);
			    joResult.put("token_create_key", DPK);
			    joResult.put("token_verify_key", IMK_AC);
			    joResult.put("err_code", "00");
			    joResult.put("err_msg", "Generate token successfull");
		    }
		    
		    else {
		    	joResult.put("err_code", "06");
		    	joResult.put("err_msg", "a-Generate Token Failed");
		    }
		    return joResult;
		 }
		
		catch(Exception e) {
			System.out.println("<GenerateTokenException> : " + e.getMessage());
	    	joResult.put("err_code", "09");
		    joResult.put("err_msg", "Tokenization System error. Please try again later");
		    return joResult;
		}
	}
	
    public static BigInteger decrypt(BigInteger ciphertext, byte [] DPK) throws Exception
    {
        FPE_Encryptor F = new FPE_Encryptor(DPK, modulus, tweak);
        BigInteger[] a_b = NumberTheory.factor(modulus);
        BigInteger a = a_b[0]; 
        BigInteger b = a_b[1];

        int r = rounds(a, b);
        BigInteger X = ciphertext;

        for (int i = 0; i != r; ++i)
        {
            BigInteger W = X.mod(a);
            BigInteger R = X.divide(a);

            BigInteger bigInteger = (W.subtract(F.F(r - i - 1, R)  ) );

            BigInteger L = bigInteger.mod(a);
            X = b.multiply(L).add(R);
        }

        return X;
    }

    public static BigInteger encrypt(BigInteger plaintext, byte [] DPK, byte [] tweak, BigInteger modulus) throws Exception
    {
        FPE_Encryptor F = new FPE_Encryptor(DPK, modulus, tweak);
        BigInteger[] a_b = NumberTheory.factor(modulus);
        BigInteger a = a_b[0]; 
        BigInteger b = a_b[1];
        int r = rounds(a, b);

        BigInteger X = plaintext;
        for (int i = 0; i != r; ++i)
        {
            BigInteger L = X.divide( b );
            BigInteger R = X.mod(b);

            BigInteger W = (L.add(F.F(i, R))).mod(a);
            X = a.multiply(R).add(W);
        }

        return X;
    }

    private static int rounds(BigInteger a, BigInteger b) throws Exception
    {
        if (a.compareTo(b) < 0 )
            throw new Exception("FPE rounds: a < b");
        return 3;
    }

    /// A simple round function based on HMAC(SHA-256)
    private static class FPE_Encryptor
    {
        private Mac mac;
        private byte[] mac_n_t;
        public FPE_Encryptor(byte[] key, BigInteger n, byte[] tweak) throws Exception
        {
            mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key, "HmacSHA256");
            mac.init(secret_key);
            byte[] n_bin = n.toByteArray();

            if (n_bin.length > MAX_N_BYTES)
                throw new Exception("N is too large for FPE encryption");

            ByteArrayOutputStream ms = new ByteArrayOutputStream();
            ms.write(n_bin.length);
            ms.write(n_bin);

            ms.write(tweak.length);
            ms.write(tweak);

            mac.reset();
            mac_n_t = mac.doFinal(ms.toByteArray());
        }

        public BigInteger F(int round_no, BigInteger R) throws IOException
        {
            byte[] r_bin = R.toByteArray();
            ByteArrayOutputStream ms = new ByteArrayOutputStream();
            ms.write(mac_n_t);
            ms.write(round_no);

            ms.write(r_bin.length);
            ms.write(r_bin);

            mac.reset();
            byte[] X = mac.doFinal(ms.toByteArray());
            
            byte[] X_ = new byte[X.length + 1];
            X_[0] = 0; // set the first byte to 0
            
            for(int i=0;i<X.length;i++){
            	X_[i+1] = X[i];
            }
            
            
            BigInteger ret = new BigInteger(X_);
            return ret;
        }
    }
    
    private static class NumberTheory
    {
    	private static final BigInteger MAX_PRIME = BigInteger.valueOf(65535);
        public static BigInteger[] factor(BigInteger n) throws Exception
        {
        	BigInteger a = BigInteger.ONE;
        	BigInteger b = BigInteger.ONE;
            
            int n_low_zero = low_zero_bits(n);

            a = a.shiftLeft(n_low_zero / 2);
            b = b.shiftLeft(n_low_zero - (n_low_zero / 2) );
            
            n = n.shiftRight(n_low_zero);
            //for (int i = 0; i != PRIMES.length; ++i)
            BigInteger prime = BigInteger.ONE;
            while(prime.compareTo(MAX_PRIME) <= 0)
            {
            	prime = prime.nextProbablePrime();
                while (n.mod(prime).compareTo(BigInteger.ZERO) == 0)
                {
                    a = a.multiply( prime );
                    if ( a.compareTo(b) > 0) {
                    	BigInteger old_b = b;
                    	b = a;
                    	a = old_b;
                    }
                    n = n.divide( prime );
                }
                if(a.compareTo(BigInteger.ONE) > 0 && b.compareTo(BigInteger.ONE) > 0 )
                {
                	break;
                }
            }

            if (a.compareTo(b) > 0) {
            	BigInteger old_b = b;
            	b = a;
            	a = old_b;
            }
            a = a.multiply(n);
            if (a.compareTo(b) < 0) {
            	BigInteger old_b = b;
            	b = a;
            	a = old_b;
            }

            if (a.compareTo(BigInteger.ONE) < 0 || b.compareTo(BigInteger.ONE) < 0) {
                throw new Exception("Could not factor n for use in FPE");
            }
            
            // return 
            return new BigInteger[]{ a,b };
        }
        
        private static int ctz(byte n)
        {
            for (int i = 0; i != 8; ++i) {
                if (((n >> i) & 0x01) > 0) {
                    return i;
                }
    		}
            return 8;
        }
        
        private static int low_zero_bits(BigInteger n)
        {
            int low_zero = 0;
            if (n.signum() > 0) {
                byte[] bytes = n.toByteArray();
                for (int i = bytes.length-1; i >=0 ; i--)
                {
                    int x =  (bytes[i] & 0xFF);
                    if (x > 0)
                    {
                        low_zero += ctz((byte)x);
                        break;
                    }
                    else
                        low_zero += 8;
                }
            }

            return low_zero;
        }
    }
    
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    
    public static void main(String[] args) throws Exception 
    {
		final byte[] key         = "Here is my key".getBytes();
		final byte[] tweak       = "12345612062018".getBytes();
		
		JSONObject jo = generateFormatPreservingToken("5576920020832616");
		String encrypted = jo.get("token").toString();
		System.out.println("Encrypted: " + encrypted);
		//BigInteger plain = new BigInteger("5576920020832616");
		//BigInteger enc   = FormatPreservingToken.encrypt(plain);
		//BigInteger dec   = FormatPreservingToken.decrypt(enc);
		
		//System.out.println("Plain : " + plain + " , Encrypted : " + enc + ", Decrypted : " + dec);
       	BigInteger bigintToken = new BigInteger(encrypted, 10);
    	BigInteger bigintResult= decrypt(bigintToken, key);
    	String decrypted       = bigintResult.toString();
    	System.out.println("Decrypted: " + decrypted);
		
	}
}
